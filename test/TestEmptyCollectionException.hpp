# pragma once
# include "../include/catch.hpp"
# include "../include/EmptyCollectionException.hpp"

TEST_CASE("Throwing EmptyCollectionException","") {

    try {
        throw EmptyCollectionException{};
    } catch (std::exception &e) {
        REQUIRE(std::string(e.what()) == "Collection is empty");
    }

}