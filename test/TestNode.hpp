# pragma once
# include "../include/catch.hpp"
# include "../include/Node.hpp"

TEST_CASE("Node class initializes from constructor","[Node<T>::Node()]") {
    Node<int> node{-10};

    REQUIRE(node.value == -10);
    REQUIRE(node.next == node_ptr<int>());
    REQUIRE(node.prev == node_ptr<int>());
    
}