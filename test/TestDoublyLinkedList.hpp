# pragma once
# include "../include/catch.hpp"
# include "../include/DoublyLinkedList.hpp"

TEST_CASE("Length of an array","[DoublyLinkedList<int>::size()]") {
    DoublyLinkedList<int> list{1,2,3,4,5};

    REQUIRE(list.size() == 5);
}

TEST_CASE("List is empty","[DoublyLinkedList<int>::is_empty()]") {

    DoublyLinkedList<int> list{1,2};

    REQUIRE(!list.is_empty());

    list.popBack();
    list.popBack();

    REQUIRE(list.is_empty());
}

TEST_CASE("adding element from the back","[DoublyLinkedList<int>::pushBack()]") {
    DoublyLinkedList<int> list{};

    list.pushBack(-32);

    REQUIRE(list.size() == 1);
    REQUIRE(list.popBack() == -32);
}

TEST_CASE("adding element from the front","[DoublyLinkedList<int>::pushFront()]") {
    DoublyLinkedList<int> list{};

    list.pushFront(-23);
    REQUIRE(list.size() == 1);

    REQUIRE(list.popFront() == -23);
    REQUIRE(list.is_empty());
}

TEST_CASE("Removing element from the back","[DoublyLinkedList<int>::popBack()]") {
    DoublyLinkedList<int> list{1,2,3,4,5};
    int result = list.popBack();
    REQUIRE(list.size() == 4);
    REQUIRE(result == 5);
}

TEST_CASE("Removing element from the front", "[DoublyLinkedList<int>::popFront()]") {
     DoublyLinkedList<int> list{1,2,3,4,5};
     list.popFront();
     REQUIRE(list.size() == 4);
     list.popFront();
     list.popFront();
     list.popFront();
     list.popFront();
     REQUIRE_THROWS_AS(*list.begin(),EmptyCollectionException);
}

TEST_CASE("Filtering elements based on a condition","[DoublyLinkedList<int>::filter()]") {
    DoublyLinkedList<int> list{1,2,3,4,5};

    auto result = list.filter([](int const& i) {
        return ((2*i)+1) == 5;
    });

    REQUIRE(result.size() == 1);
    REQUIRE(result.popBack() == 2);
    
}

TEST_CASE("Reduce operation on list","[DoublyLinkedList<int>::reduce()]") {
    DoublyLinkedList<int> list{1, 2, 3, 4, 5};

    auto result = list.reduce(0,[](int result,int const& i) {
        return result + i;
    });

    REQUIRE(result == 15);
}

