# include <iostream>
# include "../include/DoublyLinkedList.hpp"

int main() 
{
    DoublyLinkedList<int> dll{1,2,3,4,5};

    for (auto const& e : dll) {
        std::cout << e << std::endl;
    }
}