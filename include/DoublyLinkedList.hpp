# pragma once
# include <cstring>
# include <optional>
# include <functional>
# include "Node.hpp"
# include "EmptyCollectionException.hpp"

template <class T>
class DoublyLinkedList
{
    public:
        const node_ptr<T> empty_node = node_ptr<T>();

        struct iterator
        {
            private:
                node_ptr<T> head;
            
            public:
                using iterator_category = std::bidirectional_iterator_tag;
                using difference_type = std::ptrdiff_t;
                using value_type = T;
                using pointer = T*;
                using reference = T&;

                iterator(node_ptr<T> head)
                    : head(head)
                {}

                T& operator*() const {
                    if (head == node_ptr<T>()) {
                        throw EmptyCollectionException("List is empty");
                    }
                    return head->value;
                }

                iterator &operator++() {
                   head = head->next;
                   return *this; 
                }

                iterator operator++(int) {
                    iterator tmp = *this;
                    ++(*this);
                    return tmp;
                }

                iterator &operator--() {
                    head = head->prev;
                    return *this;
                }

                iterator operator--(int) {
                    iterator tmp = *this;
                    --(*this);
                    return tmp;
                }

                friend bool operator==(iterator const& a,iterator const& b) {
                    return a.head == b.head;
                }

                friend bool operator!=(iterator const& a,iterator const& b) {
                    return a.head != b.head;
                }
        };

        iterator begin() {
            return iterator(head);
        }

        iterator end() {
            return iterator(node_ptr<T>());
        }



    private:
        std::size_t length = 0;
        std::shared_ptr<Node<T>> head = empty_node;
        std::shared_ptr<Node<T>> tail = empty_node;

    public:

        DoublyLinkedList()
        {}

        DoublyLinkedList(std::initializer_list<T> list) {
            for (auto const& element : list) {
                pushBack(element);
            }
        }

        DoublyLinkedList(DoublyLinkedList<T>&& list)
            : length(std::exchange(list.length,0)),
              head(std::exchange(list.head,empty_node)),
              tail(std::exchange(list.tail,empty_node))
        {}

        DoublyLinkedList(DoublyLinkedList<T> const& ) = delete;

        virtual ~DoublyLinkedList() noexcept = default;

        void clear() {
            this->~DoublyLinkedList();
            this->length = 0;
        }

        std::size_t size() {
            return length;
        }

        bool is_empty() {
            return head == empty_node;
        }

        void pushBack(T const& value)
        {
            if (length == 0) {

                head = node_ptr<T>(new Node<T>(value));
                tail = head;
            }

            else {
                node_ptr<T> new_node = node_ptr<T>(new Node<T>(value));

                node_ptr<T> intermediate = tail;
                intermediate->next = new_node;
                intermediate->prev = tail->prev;

                new_node->prev = intermediate;
                tail = new_node;
            }

            length += 1;
        }

        void pushFront(T const& value) {

            auto new_head = node_ptr<T>(new Node<T>(value));

            if (is_empty()) {
                pushBack(value);
                return;
            }

            head->prev = new_head;
            new_head->next = head;
            head = new_head;

            length += 1;
        }

        T popFront() {

            if (is_empty()) {
                throw EmptyCollectionException{"List is empty"};
            }

            node_ptr<T> node_to_delete = head;

            if (node_to_delete->next == empty_node) {
                T value = node_to_delete->value;
                head = tail = empty_node;
                node_to_delete.reset();
                return value; 
            }

            head = node_to_delete->next;
            head->prev = empty_node;

            auto return_value = node_to_delete->value;
            node_to_delete.reset();

            length -= 1;

            return return_value;
        }

        T popBack() {

            if (is_empty()) {
                throw EmptyCollectionException("List is empty");
            }

            node_ptr<T> penultimate = tail->prev;

            if (penultimate == empty_node) {
                T value = tail->value;
                tail.reset();
                tail = empty_node;
                head = empty_node;
                return value;
            }

            penultimate->next = empty_node;
            auto return_value = tail->value;
    
            tail.reset();
            tail = penultimate;
            length -= 1;
            
            return return_value;
        }

        bool operator==(DoublyLinkedList<T> const& list) {

            if (length != list.length) {
                return false;
            }

            auto this_list = begin();
            auto that_list = list.begin();

            while ((this_list != end()) && (that_list != list.end())) {
                if (*this_list != *that_list) {
                    return false;
                }
                ++this_list;
                ++that_list;
            }

            return true;
        }

        bool operator!=(DoublyLinkedList<T> const& list) {
            return !operator==(list);
        }

        friend std::ostream &operator<<(std::ostream &oss, DoublyLinkedList<T> const& ll) {

            auto h = ll.head;
            
            if(h == ll.empty_node) {
                return oss;
            }

            while (h != ll.empty_node) {
                oss << h->value << " --> ";
                h = h->next;
            }

            return oss;
        }

        DoublyLinkedList<T> filter(std::function<bool(const T&)> predicate) {

            DoublyLinkedList<T> dll{};
            for(auto const& e : *this)
            {
                if (predicate(e)) {
                    dll.pushBack(e);
                }
            }

            return dll;
        }

        template <typename R>
        DoublyLinkedList<R> map(std::function<R(T const& )> func)
        {
            DoublyLinkedList<R> dll{};

            for (auto const& e : *this) {
                auto result = func(e);
                dll.pushBack(result);
            }

            return dll;
        }

        T reduce(T identity, std::function<T(T, T const&)> accumulator) {

            pushFront(identity);
            T result = reduce(accumulator);
            popFront();

            return result;
        }

    T reduce(std::function<T(T, T const&)> accumulator)
    {
        if (is_empty()) {
        throw EmptyCollectionException("List is empty");
        }
        
        T result{};
        for (auto it = this->begin(); it != this->end(); ++it)
        {
            result = accumulator(result,*it);
        }

        return result;
    }

    std::optional<T> find_unique(std::function<bool(T const&)> predicate)
    {

        for(auto const& e : *this) {
            if (predicate(e)) {
                return e;
            }
        }

        return std::nullopt;
    }
};