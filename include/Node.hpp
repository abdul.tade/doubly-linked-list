# pragma once
# include <memory>

template <class T>
struct Node {

    public:

        T value;
        std::shared_ptr<Node<T>> prev;
        std::shared_ptr<Node<T>> next;

        Node() = default;

        Node(T const& value)
            : value(value),
              prev(std::shared_ptr<Node<T>>()),
              next(std::shared_ptr<Node<T>>())
        {}

        virtual ~Node() = default;
};

template <typename T>
using node_ptr = std::shared_ptr<Node<T>>;